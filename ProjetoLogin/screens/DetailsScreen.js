import React from 'react';
import { StyleSheet, View,Text, AsyncStorage } from 'react-native';
import { Card} from "react-native-elements";

export default class DetailsScreen extends React.Component {
    static navigationOptions =  {
        headerTransparent: false,
    }

    state = {
       user: [],
    }

    componentDidMount = async ()=> {
        const userBanco = JSON.parse(await AsyncStorage.getItem("@ProjetoLogin::user"));
        this.setState({user: userBanco});
    }
    
    render() {
        return (
            <View style={styles.container}>
                <Card>
                    <Text style={styles.descricao}>Detalhes do usuário</Text> 
                </Card>
                <Card>
                    <Text style={styles.descricao}>Email:</Text> 
                    <Text>{this.state.user.email}</Text> 
                </Card>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        margin: 10,
    }, 
    label: {
        fontSize: 23,
        fontWeight: 'bold',
    },
    descricao: {
        fontSize: 20,
        fontWeight: 'bold',
    },
})
