import React from 'react';
import { StyleSheet, View, ActivityIndicator,AsyncStorage } from 'react-native';

export default class LogoutScreen extends React.Component {

    async componentDidMount() {
        await AsyncStorage.removeItem("@ProjetoLogin::user");
        //this.props.navigation.navigate('Login');
        //
        this.props.navigation.navigate('Login');
    };

    render() {
        return (
            <View style={[styles.container, styles.horizontal]}>
                 <ActivityIndicator size="large" color="#C0C0C0" />
             </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 2,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
})