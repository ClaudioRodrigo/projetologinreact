import React from 'react';
import { StyleSheet, View,Text, Image, TouchableOpacity} from 'react-native';

export default class HomeScreen extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <Image 
                    source={require('../icons/account-circle-outline.png')} 
                    style={{ padding: 40, width: 50, height: 50}}
                />
                <Text style={styles.descricao}>Bem vindo</Text>
                <Text style={styles.descricao}>Arraste com o dedo </Text>
                <Text style={styles.descricao}>da esquerda para a  </Text>
                <Text style={styles.descricao}>direita para aparecer o Menu </Text>

                <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('Details')}>
                    <Text style={styles.descricao}> Detalhar usuário </Text>
                </TouchableOpacity>

            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 2,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        margin: 10,
    },  
    descricao: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    button: {
        marginTop: 20,
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding:10
    },
})
