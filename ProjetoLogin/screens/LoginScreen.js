import React from 'react';
import { StyleSheet, View, AsyncStorage, Alert, Image} from 'react-native';
import { Card, Button, FormLabel, FormInput } from "react-native-elements";

export default class LoginScreen extends React.Component {

    state = {    
        email: '',
        password: '',
    }

    autenticar = async () =>{

        if(this.state.password.trim()  == 0 && this.state.email.trim() == 0){
            Alert.alert("Aviso","Preencha os campos email e/ou senha");
        }else if(this.state.password == "123456"){
            const user = {
                email: this.state.email,
                password: this.state.password,
            }
           
            await AsyncStorage.setItem("@ProjetoLogin::user", JSON.stringify(user));
            this.props.navigation.navigate('RootStack');
        }else {
            Alert.alert("Aviso","Email e/ou senha incorretas");
        }
    }

    render() {
        return (
                <View style={ styles.container}> 
                   <Card>
                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center'}}>
                        <Image 
                            source={require('../icons/account-circle-outline.png')} 
                            style={{ padding: 20, width: 45, height: 45}}
                        />
                    </View>                  
                    <FormLabel>E-mail</FormLabel>
                    <FormInput value={this.state.email} onChangeText={email => this.setState({email})} placeholder="Digite seu e-mail" />
                    <FormLabel>Senha</FormLabel>
                    <FormInput onChangeText={password => this.setState({password})} secureTextEntry placeholder="Digite sua senha" />

                    <Button
                        onPress={this.autenticar} 
                        buttonStyle={{ marginTop: 20 }}
                        backgroundColor="#03A9F4"
                        title="Entrar"
                    />
                    </Card>
                </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        justifyContent: 'center',
        paddingVertical: 20,
    },    
})
