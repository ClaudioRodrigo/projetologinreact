import React from 'react';
import { StyleSheet, View, TextInput, Text, Image, TouchableOpacity, AsyncStorage, ActivityIndicator } from 'react-native';

export default class LoginScreen extends React.Component {

    async componentDidMount() {
        const user = JSON.parse(await AsyncStorage.getItem("@ProjetoLogin::user"));

        if(user){
            this.props.navigation.navigate('RootStack');
        }else{
            this.props.navigation.navigate('Login'); 
        }
    };

    render() {
        return (
            <View style={[styles.container, styles.horizontal]}>
                <ActivityIndicator size="large" color="#C0C0C0" />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
})

