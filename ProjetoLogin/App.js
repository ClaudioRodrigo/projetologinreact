
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { createDrawerNavigator, createStackNavigator, createSwitchNavigator} from 'react-navigation';
import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';
import LoadingLoginScreen from './screens/LoadingLoginScreen';
import DetailsScreen from './screens/DetailsScreen';
import LogoutScreen from './screens/LogoutScreen';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <RootSwitch />
    );
  }
}

const RootDrawer = createDrawerNavigator(
  {
    Home: HomeScreen,
    Logout: LogoutScreen,
  },
  {
    initialRoutName: 'Home',
  }
);

const RootStack = createStackNavigator(
  {
    RootDrawer: RootDrawer,
    Details: DetailsScreen,
  },
  {
    initialRoutName: 'RootDrawer',
    navigationOptions: {
      headerTransparent: true,
    }
  },
);

const RootSwitch = createSwitchNavigator(
  {
    LoadingLogin: LoadingLoginScreen,
    Login: LoginScreen,
    RootStack: RootStack
  },
  {
    initialRoutName: 'LoadingLogin',
  }
);


